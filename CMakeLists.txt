cmake_minimum_required(VERSION 3.10.0)
project(QCBox VERSION 0.1.0)


add_executable(QCBox src/main.c
                     src/sys.c
                     src/builtins.c)

target_include_directories(QCBox PUBLIC src/libs/qclib)
target_link_directories(QCBox PUBLIC src/libs/qclib)

target_link_libraries(QCBox -lz -l:qcvm.a -lm -lz)
