
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "sys.h"
#include "builtins.h"

#include "progtype.h"
#include "progslib.h"



void runVM(const char* progsName)
{
    pubprogfuncs_t *progFuncs;
    func_t func;
    progsnum_t progsNum;

    progexterns_t progExterns;
    memset(&progExterns, 0, sizeof(progExterns));

    progExterns.progsversion = PROGSTRUCT_VERSION;
    progExterns.ReadFile = Sys_ReadFile;
    progExterns.FileSize = Sys_FileSize;
    progExterns.Abort = Sys_Abort;
    progExterns.Printf = printf;

    progExterns.numglobalbuiltins = builtinCount;
	progExterns.globalbuiltins = builtins;

    progFuncs = InitProgs(&progExterns);
    progFuncs->Configure(progFuncs, 1024 * 1024, 1, 0); // memory quantity of 1mb. Maximum progs loadable into the instance of 1
    // If you support multiple progs types, you should tell the VM the offsets here, via RegisterFieldVar

    progsNum = progFuncs->LoadProgs(progFuncs, progsName); // load the progs.
    if (progsNum < 0)
    {
        printf("Failed to load progs \"%s\"\n", progsName);
        progFuncs->Shutdown(progFuncs);
        return;
    }

    // allocate qc-acessable strings here for 64bit cpus. (allocate via AddString, tempstringbase is a holding area not used by the actual vm)
    // you can call functions before InitEnts if you want. it's not really advised for anything except naming additional progs. This sample only allows one max.

    progFuncs->InitEnts(progFuncs, 10); // Now we know how many fields required, we can say how many maximum ents we want to allow. 10 in this case. This can be huge without too many problems.

    // now it's safe to ED_Alloc.

    func = progFuncs->FindFunction(progFuncs, "main", PR_ANY); // find the function 'main' in the first progs that has it.
    if (!func)
        printf("Couldn't find function\n");
    else
        progFuncs->ExecuteProgram(progFuncs, func); // call the function

    progFuncs->Shutdown(progFuncs);
}

int main(int argc, const char **argv)
{
    const char* progsName;

    if (argc < 2)
    {
        progsName = "progs.dat";
    }
    else
    {
        progsName = argv[1];
    }
    
    runVM(progsName);
    
    return 0;
}
