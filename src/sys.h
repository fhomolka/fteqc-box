#ifndef QC_BOX_SYS_H
#define QC_BOX_SYS_H
#include "progslib.h"

//Taken from example file - test.c
void *PDECL Sys_ReadFile(const char *fname, unsigned char *(PDECL *buf_get)(void *ctx, size_t len), void *buf_ctx, size_t *out_size, pbool issourcefile);
int Sys_FileSize (const char *fname);
pbool Sys_WriteFile (const char *fname, void *data, int len);
void Sys_Abort(char *s, ...);

#endif