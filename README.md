# QCBox

Built on top of [FTEQW QCLib](https://github.com/fte-team/fteqw/tree/master/engine/qclib).

## Why?

I wanted a small playground for QuakeC.

## Building

### Windows
It's currently a bit weird to build on Windows, you need MinGW.

### Linux
Enter the qclib directory and run `make qcvm.a`
Then, run cmake, with the build directory set wherever you want.

## Running

QCBox will try to look for `progs.dat` unless a name is passed to it
